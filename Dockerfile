FROM node:14.0.0

COPY . /app/

WORKDIR /app

RUN npm install

EXPOSE 3000

ENTRYPOINT [ "npm", "start" ]
